import React, {Component} from 'react'
import Grid  from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import * as contentful from 'contentful'
import Course from '../components/Course'

const SPACE_ID = 'vx1btnthptop'
const ACCESS_TOKEN = '52cd361c84c9aedd9f4e9840a3c8969c27cf1cbf2eb5056257375708f895b4f5'

const client = contentful.createClient({
    space: SPACE_ID,
    accessToken: ACCESS_TOKEN
})

class CourseList extends Component {
    state = {
        coorses: [{}, {}],
        searchString: ''
    }

    constructor() {
        super()
        this.getCourses()
    }

    getCourses = () => {
        /*
        client.getEntries({
            content_type: 'course',
            query: this.state.searchString
        })
        .then((response) => {
            this.setState({courses: response.items})
        })
        .catch((error) => {
            console.log("Error fetch")
            console.log(error)
        })
        */
       /*
       this.setState({courses: [
           {
               name: "Course1"
           }
       ]})
       */
       
    }

    onSearchInputChange = (event) => {
        if (event.target.value) {
            this.setState({searchString: event.target.value})
        } else {
            this.setState({searchString: ''})
        }

        this.getCourses()
    }

    render() {
        return (
            <div>
                {this.state.coorses ? (
                    <div>
                        <TextField style={{padding: 24}}
                            id="searchInput"
                            margin="normal"
                            onChange={this.onSearchInputChange} />
                        <Grid container spacing={24} style={{padding: 24}}>
                            {this.state.coorses.map(currentCourse => (
                                <Grid item xs={12} sm={6} lg={4} xl={3}>
                                    <Course course={currentCourse} />
                                </Grid>
                            ))}
                        </Grid>
                    </div>


                ) : "No courses"}
            </div>
        )
    }
}

export default CourseList;